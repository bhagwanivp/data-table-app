import React from "react";

export const columns = [
	{
		key: "1",
		title: "Identifier",
		dataIndex: "identifier",
		// fixed: "left",
		width: 90,
		render: identifier => identifier.split(":")[1],
		sorter: (a, b) =>
			Number(a.identifier.split(":")[1]) -
			Number(b.identifier.split(":")[1])
	},
	{
		key: "2",
		title: "Title",
		dataIndex: "title",
		width: 350,
		ellipsis: true,
		sorter: (a, b) => a.title.length - b.title.length
	},
	{
		key: "3",
		title: "Access Level",
		dataIndex: "accesslevel",
		width: 70
		// sorter: (a, b) => a.accesslevel - b.accesslevel,
		// sortDirections: ["descend", "ascend"]
	},
	{
		key: "4",
		title: "modified",
		dataIndex: "modified",
		width: 120,
		filters: Array.from(Array(10), (_, i) => ({
			text: String(2010 + i),
			value: String(2010 + i)
		})),
		onFilter: (value, record) => record.modified.split("-")[0] === value,
		sorter: (a, b) => a.modified.split("-")[0] - b.modified.split("-")[0]
	},
	{
		key: "5",
		title: "Publisher Name",
		dataIndex: "publisher_name",
		width: 300,
		ellipsis: true
	},
	{
		key: "6",
		title: "Publisher Type",
		dataIndex: "publisher_type"
	},
	{
		key: "7",
		title: "Accrual Periodicity",
		dataIndex: "accrualperiodicity",
		width: 100
	},
	{
		key: "8",
		title: "Email",
		dataIndex: "contactpoint_hasemail",
		width: 200,
		// fixed: "right",
		render: contactpoint_hasemail => (
			<a href={contactpoint_hasemail}>
				{contactpoint_hasemail.split(":")[1]}
			</a>
		)
	}
	// {
	// 	key: "9",
	// 	title: "Type",
	// 	dataIndex: "type"
	// }
];

export default {
	columns
};
