import React from "react";

const ExpandedView = ({
	title,
	identifier,
	accesslevel,
	temporal,
	description,
	modified,
	publisher_name,
	publisher_type,
	contactpoint_type,
	contactpoint_fn,
	contactpoint_hasemail,
	accrualperiodicity,
	spatial
}) => (
	<p className="expanded-view-wrapper">
		<h3>Title</h3>
		<p>{title}</p>

		<h3>Description</h3>
		<p>{description}</p>

		<h3>Identifier</h3>
		<p>{identifier}</p>

		<h3>Access Level</h3>
		<p>{accesslevel}</p>

		<h3>Temporal</h3>
		<p>{temporal}</p>

		<h3>Modified on</h3>
		<p>{modified}</p>

		<h3>Publisher Name</h3>
		<p>{publisher_name}</p>

		<h3>Publisher Type</h3>
		<p>{publisher_type}</p>

		<h3>Contact Point Fn</h3>
		<p>{contactpoint_fn}</p>

		<h3>Contact Point Type</h3>
		<p>{contactpoint_type}</p>

		<h3>Contact Point Email</h3>
		<p>
			<a href={contactpoint_hasemail}>
				{contactpoint_hasemail.split(":")[1]}
			</a>
		</p>

		<h3>Accrual Periodicity</h3>
		<p>{accrualperiodicity}</p>

		<h3>Spatial Type</h3>
		<p>{spatial.type}</p>

		<h3>Spatial Coordinates</h3>
		<p>{JSON.stringify(spatial.coordinates)}</p>
	</p>
);

export default ExpandedView;
