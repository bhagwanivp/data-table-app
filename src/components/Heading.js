import React from "react";

const Heading = () => (
	<p className="heading">
		<a
			href="https://gitlab.com/bhagwanivp/data-table-app"
			target="_blank"
			rel="noopener noreferrer">
			Assignment submission
		</a>{" "}
		for Ayko by{" "}
		<a
			href="https://www.linkedin.com/in/vivekbhagwani/"
			target="_blank"
			rel="noopener noreferrer">
			Vivek Bhagwani
		</a>
	</p>
);

export default Heading;
