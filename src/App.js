import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./App.scss";
import { columns } from "./constants";
import ExpandedView from "./components/ExpandedView";
import { Table } from "antd";
import Heading from "./components/Heading";

const App = () => {
	const [data, setData] = useState([]);

	const getData = () => {
		const jsonData = require("./data.json").data.map((obj, i) => {
			return { key: String(i), ...obj };
		});
		setData(jsonData);
		/* fetch("http://localhost:3333/data")
			.then(res => res.json())
			.then(res => {
				const newData = res.map((obj, i) => {
					return { key: String(i), ...obj };
				});
				setData(newData);
			}); */
	};

	useEffect(() => {
		if (data.length === 0) getData();
	}, [data]);

	return (
		<div className="App">
			<header className="App-header">
				<Heading />
				{data.length && (
					<Table
						size="small"
						columns={columns}
						dataSource={data}
						rowClassName={(_, index) =>
							index % 2 === 0
								? "table-row-light"
								: "table-row-dark"
						}
						expandable={{
							expandedRowRender: ExpandedView
						}}
						scroll={{ y: 550 }}
						pagination={{ position: ["topRight", "bottomRight"] }}
					/>
				)}
				{!data.length && <p>No data found.</p>}
			</header>
		</div>
	);
};

export default App;
